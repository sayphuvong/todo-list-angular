# To create this project
1. Install Angular CLI `npm install -g @angular/cli`.
2. Create Angular Project with @angular/cli `ng new todo-list-angular`.

# Install awesome Extension for vsCode
1. Angular Language Service (to highlight the syntax, and bla...bla...).
2. Angular Support (to goto Definition a faster way, and bla...bla...).

# Some keyword for newbie
- `*ngIf`, `*ngFor` : is called the `DIRECTIVE` or a built-in Angular directive.
- `#newItem` as attribute of a element in the html template is specify a model.
- If you need to get element-ref of input, just only use `@ViewChild` from @angular/core.